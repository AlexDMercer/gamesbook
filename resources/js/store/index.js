import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import moment from 'moment'
Vue.use(Vuex)



let store = new Vuex.Store({
   state: {
      teams: [],
      lastGames: [],
      games: [],
      team: {}
   },
   getters: {
      teams(state) {
         return state.teams
      },
      lastGames(state) {
         return state.lastGames
      },
      games(state) {
         return state.games
      },
      team(state) {
         return state.team
      }
   },
   actions: {
      createGame({ commit }, game) {
         axios.post('api/games', game).then(res => {
            commit('newGame', res.data)
         })
      },
      getTeams({ commit }) {
         axios.get("api/teams").then(res => {
            commit('setTeams', res.data.teams)
         })
      },
      getTeamGames({ commit }, name) {
         axios.get("api/teams/" + name, { name }).then(res => {
            commit('setTeamGames', res.data.games)
            commit('setTeam', res.data.team)
         })
      },
      getLastGames({ commit }) {
         axios.get("api/games").then(res => {
            commit('setLastGames', res.data)
         })
      }
   },
   mutations: {
      newGame(state, game) {
         game.created_at = moment(game.created_at).format("LL")

         state.lastGames.unshift(game)
      },
      setTeams(state, teams) {
         state.teams = teams
      },
      setLastGames(state, games) {
         let format = games.map(g => {
            moment.locale('en')
            g.created_at = moment(g.created_at).format("LL")
            return g
         })

         state.lastGames = format
      },
      setTeamGames(state, games) {
         let format = games.map(g => {
            moment.locale('en')
            g.created_at = moment(g.created_at).format("LL")
            return g
         })

         state.games = format
      },
      setTeam(state, team) {
         state.team = team
      }
   }
})

export default store