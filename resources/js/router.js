import Vue from 'vue'
import vueRouter from 'vue-router'
Vue.use(vueRouter)

export default new vueRouter({
   mode: "history",
   base: process.env.BASE_URL,
   routes: [
      {
         path: '/',
         name: 'home',
         component: () => import('./components/pages/home'),
      },
      {
         path: '/:name',
         name: 'games',
         component: () => import('./components/pages/games'),
      },
   ]
})