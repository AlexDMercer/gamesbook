Project info

=========================
Php version: 8.0.1
Node js version: 12.18.4
npm version: 6.14.6

Laravel version: 8.32.1
Vue js version: 2
=========================


To start a project:

php artisan migrate
php artisan db:seed
php artisan serve