<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Models\Team;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $games = Game::latest()->get();

        return response()->json($games);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $game = Game::create([
            'team_1' => $request->team_1,
            'team_2' => $request->team_2,
            'goal_1' => $request->goal_1,
            'goal_2' => $request->goal_2
        ]);

        $team_1 = Team::where('name', $request->team_1)->first();
        $team_2 = Team::where('name', $request->team_2)->first();
        $game->team()->attach($team_1->id);
        $game->team()->attach($team_2->id);

        $goal_1 = $team_1->goal + $request->goal_1;
        $fail_1 = $team_1->fail + $request->goal_2;
        $goal_2 = $team_2->goal + $request->goal_2;
        $fail_2 = $team_2->fail + $request->goal_1;

        $team_1->update(['goal' => $goal_1, 'fail' => $fail_1]);
        $team_2->update(['goal' => $goal_2, 'fail' => $fail_2]);

        return response()->json($game);
    }
}
