<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\GameController;
use App\Http\Controllers\API\TeamController;

Route::get('/teams', 'App\Http\Controllers\Api\TeamController@index');
Route::get('/teams/{name}', 'App\Http\Controllers\Api\TeamController@show');

Route::get('/games', 'App\Http\Controllers\Api\GameController@index');
Route::post('/games', 'App\Http\Controllers\Api\GameController@store');
