<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        DB::table('teams')->insert([
            'name' => 'Spartak',
            'goal' => 0,
            'fail' => 0,
            'created_at' => now()
        ]);
        DB::table('teams')->insert([
            'name' => 'Dinamo',
            'goal' => 0,
            'fail' => 0,
            'created_at' => now()
        ]);
        DB::table('teams')->insert([
            'name' => 'Lokomotiv',
            'goal' => 0,
            'fail' => 0,
            'created_at' => now()
        ]);
        DB::table('teams')->insert([
            'name' => 'Gazmyas',
            'goal' => 0,
            'fail' => 0,
            'created_at' => now()
        ]);
    }
}
